```
{
  "version": 4,
  "terraform_version": "1.0.6",
  "serial": 245,
  "lineage": "6112403d-23e4-6975-871f-69a64798c9ff",
  "outputs": {},
  "resources": [
    {
      "mode": "managed",
      "type": "aws_eip",
      "name": "public_eip",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "address": null,
            "allocation_id": "eipalloc-0b10ac5a566caf2a2",
            "associate_with_private_ip": "10.0.1.100",
            "association_id": "eipassoc-005f5b0c39024a992",
            "carrier_ip": "",
            "customer_owned_ip": "",
            "customer_owned_ipv4_pool": "",
            "domain": "vpc",
            "id": "eipalloc-0b10ac5a566caf2a2",
            "instance": "",
            "network_border_group": "ap-southeast-1",
            "network_interface": "eni-07e5b8e7d2fea4a5f",
            "private_dns": "ip-10-0-1-100.ap-southeast-1.compute.internal",
            "private_ip": "10.0.1.100",
            "public_dns": "ec2-18-136-33-179.ap-southeast-1.compute.amazonaws.com",
            "public_ip": "18.136.33.179",
            "public_ipv4_pool": "amazon",
            "tags": null,
            "tags_all": {},
            "timeouts": null,
            "vpc": true
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiZGVsZXRlIjoxODAwMDAwMDAwMDAsInJlYWQiOjkwMDAwMDAwMDAwMCwidXBkYXRlIjozMDAwMDAwMDAwMDB9fQ==",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_network_interface.public_frontend_eni",
            "aws_security_group.public_sg",
            "aws_subnet.public_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_instance",
      "name": "private_ec2_backend",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "ami": "ami-055d15d9cfddf7bd3",
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:instance/i-0a9aa1352780001de",
            "associate_public_ip_address": false,
            "availability_zone": "ap-southeast-1b",
            "capacity_reservation_specification": [
              {
                "capacity_reservation_preference": "open",
                "capacity_reservation_target": []
              }
            ],
            "cpu_core_count": 1,
            "cpu_threads_per_core": 2,
            "credit_specification": [
              {
                "cpu_credits": "unlimited"
              }
            ],
            "disable_api_termination": false,
            "ebs_block_device": [],
            "ebs_optimized": false,
            "enclave_options": [
              {
                "enabled": false
              }
            ],
            "ephemeral_block_device": [],
            "get_password_data": false,
            "hibernation": false,
            "host_id": null,
            "iam_instance_profile": "",
            "id": "i-0a9aa1352780001de",
            "instance_initiated_shutdown_behavior": "stop",
            "instance_state": "running",
            "instance_type": "t3.micro",
            "ipv6_address_count": 0,
            "ipv6_addresses": [],
            "key_name": "aws-keypair-alex",
            "launch_template": [],
            "metadata_options": [
              {
                "http_endpoint": "enabled",
                "http_put_response_hop_limit": 1,
                "http_tokens": "optional",
                "instance_metadata_tags": "disabled"
              }
            ],
            "monitoring": false,
            "network_interface": [
              {
                "delete_on_termination": false,
                "device_index": 0,
                "network_interface_id": "eni-01ef04d86b620823f"
              }
            ],
            "outpost_arn": "",
            "password_data": "",
            "placement_group": "",
            "placement_partition_number": null,
            "primary_network_interface_id": "eni-01ef04d86b620823f",
            "private_dns": "ip-10-0-2-100.ap-southeast-1.compute.internal",
            "private_ip": "10.0.2.100",
            "public_dns": "",
            "public_ip": "",
            "root_block_device": [
              {
                "delete_on_termination": true,
                "device_name": "/dev/sda1",
                "encrypted": false,
                "iops": 100,
                "kms_key_id": "",
                "tags": {},
                "throughput": 0,
                "volume_id": "vol-0313c9ec586b4a9dc",
                "volume_size": 8,
                "volume_type": "gp2"
              }
            ],
            "secondary_private_ips": [],
            "security_groups": [],
            "source_dest_check": true,
            "subnet_id": "subnet-055b001d04c88d3f8",
            "tags": {
              "Name": "private_ec2_backend"
            },
            "tags_all": {
              "Name": "private_ec2_backend"
            },
            "tenancy": "default",
            "timeouts": null,
            "user_data": null,
            "user_data_base64": null,
            "volume_tags": null,
            "vpc_security_group_ids": [
              "sg-022088b490b9408f7"
            ]
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMCwidXBkYXRlIjo2MDAwMDAwMDAwMDB9LCJzY2hlbWFfdmVyc2lvbiI6IjEifQ==",
          "dependencies": [
            "aws_network_interface.private_backend_eni",
            "aws_security_group.private_sg_backend",
            "aws_subnet.private_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_instance",
      "name": "private_ec2_db",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "ami": "ami-055d15d9cfddf7bd3",
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:instance/i-0e4a092105438d93b",
            "associate_public_ip_address": false,
            "availability_zone": "ap-southeast-1b",
            "capacity_reservation_specification": [
              {
                "capacity_reservation_preference": "open",
                "capacity_reservation_target": []
              }
            ],
            "cpu_core_count": 1,
            "cpu_threads_per_core": 2,
            "credit_specification": [
              {
                "cpu_credits": "unlimited"
              }
            ],
            "disable_api_termination": false,
            "ebs_block_device": [],
            "ebs_optimized": false,
            "enclave_options": [
              {
                "enabled": false
              }
            ],
            "ephemeral_block_device": [],
            "get_password_data": false,
            "hibernation": false,
            "host_id": null,
            "iam_instance_profile": "",
            "id": "i-0e4a092105438d93b",
            "instance_initiated_shutdown_behavior": "stop",
            "instance_state": "running",
            "instance_type": "t3.micro",
            "ipv6_address_count": 0,
            "ipv6_addresses": [],
            "key_name": "aws-keypair-alex",
            "launch_template": [],
            "metadata_options": [
              {
                "http_endpoint": "enabled",
                "http_put_response_hop_limit": 1,
                "http_tokens": "optional",
                "instance_metadata_tags": "disabled"
              }
            ],
            "monitoring": false,
            "network_interface": [
              {
                "delete_on_termination": false,
                "device_index": 0,
                "network_interface_id": "eni-07d4bd086558a35cf"
              }
            ],
            "outpost_arn": "",
            "password_data": "",
            "placement_group": "",
            "placement_partition_number": null,
            "primary_network_interface_id": "eni-07d4bd086558a35cf",
            "private_dns": "ip-10-0-2-101.ap-southeast-1.compute.internal",
            "private_ip": "10.0.2.101",
            "public_dns": "",
            "public_ip": "",
            "root_block_device": [
              {
                "delete_on_termination": true,
                "device_name": "/dev/sda1",
                "encrypted": false,
                "iops": 100,
                "kms_key_id": "",
                "tags": {},
                "throughput": 0,
                "volume_id": "vol-0024fccd210b02c10",
                "volume_size": 8,
                "volume_type": "gp2"
              }
            ],
            "secondary_private_ips": [],
            "security_groups": [],
            "source_dest_check": true,
            "subnet_id": "subnet-055b001d04c88d3f8",
            "tags": {
              "Name": "private_ec2_db"
            },
            "tags_all": {
              "Name": "private_ec2_db"
            },
            "tenancy": "default",
            "timeouts": null,
            "user_data": null,
            "user_data_base64": null,
            "volume_tags": null,
            "vpc_security_group_ids": [
              "sg-0c9c6b9b6ef85e094"
            ]
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMCwidXBkYXRlIjo2MDAwMDAwMDAwMDB9LCJzY2hlbWFfdmVyc2lvbiI6IjEifQ==",
          "dependencies": [
            "aws_network_interface.private_db_eni",
            "aws_security_group.private_sg_backend",
            "aws_security_group.private_sg_db",
            "aws_subnet.private_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_instance",
      "name": "public_ec2_frontend",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "ami": "ami-055d15d9cfddf7bd3",
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:instance/i-0dfbca2a0f258c790",
            "associate_public_ip_address": true,
            "availability_zone": "ap-southeast-1b",
            "capacity_reservation_specification": [
              {
                "capacity_reservation_preference": "open",
                "capacity_reservation_target": []
              }
            ],
            "cpu_core_count": 1,
            "cpu_threads_per_core": 2,
            "credit_specification": [
              {
                "cpu_credits": "unlimited"
              }
            ],
            "disable_api_termination": false,
            "ebs_block_device": [],
            "ebs_optimized": false,
            "enclave_options": [
              {
                "enabled": false
              }
            ],
            "ephemeral_block_device": [],
            "get_password_data": false,
            "hibernation": false,
            "host_id": null,
            "iam_instance_profile": "",
            "id": "i-0dfbca2a0f258c790",
            "instance_initiated_shutdown_behavior": "stop",
            "instance_state": "running",
            "instance_type": "t3.micro",
            "ipv6_address_count": 0,
            "ipv6_addresses": [],
            "key_name": "aws-keypair-alex",
            "launch_template": [],
            "metadata_options": [
              {
                "http_endpoint": "enabled",
                "http_put_response_hop_limit": 1,
                "http_tokens": "optional",
                "instance_metadata_tags": "disabled"
              }
            ],
            "monitoring": false,
            "network_interface": [
              {
                "delete_on_termination": false,
                "device_index": 0,
                "network_interface_id": "eni-07e5b8e7d2fea4a5f"
              }
            ],
            "outpost_arn": "",
            "password_data": "",
            "placement_group": "",
            "placement_partition_number": null,
            "primary_network_interface_id": "eni-07e5b8e7d2fea4a5f",
            "private_dns": "ip-10-0-1-100.ap-southeast-1.compute.internal",
            "private_ip": "10.0.1.100",
            "public_dns": "",
            "public_ip": "18.136.33.179",
            "root_block_device": [
              {
                "delete_on_termination": true,
                "device_name": "/dev/sda1",
                "encrypted": false,
                "iops": 100,
                "kms_key_id": "",
                "tags": {},
                "throughput": 0,
                "volume_id": "vol-03a2b00ea15e0eab0",
                "volume_size": 8,
                "volume_type": "gp2"
              }
            ],
            "secondary_private_ips": [],
            "security_groups": [],
            "source_dest_check": true,
            "subnet_id": "subnet-02e521fe0f56d67b6",
            "tags": {
              "Name": "public_ec2_frontend"
            },
            "tags_all": {
              "Name": "public_ec2_frontend"
            },
            "tenancy": "default",
            "timeouts": null,
            "user_data": null,
            "user_data_base64": null,
            "volume_tags": null,
            "vpc_security_group_ids": [
              "sg-027341e29aaa542b7"
            ]
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMCwidXBkYXRlIjo2MDAwMDAwMDAwMDB9LCJzY2hlbWFfdmVyc2lvbiI6IjEifQ==",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_network_interface.public_frontend_eni",
            "aws_security_group.public_sg",
            "aws_subnet.public_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_internet_gateway",
      "name": "nat_gw",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:internet-gateway/igw-041c43cb10c8856bf",
            "id": "igw-041c43cb10c8856bf",
            "owner_id": "741286671880",
            "tags": {
              "Name": "my_vpc-nat_gw"
            },
            "tags_all": {
              "Name": "my_vpc-nat_gw"
            },
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "bnVsbA==",
          "dependencies": [
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_network_interface",
      "name": "private_backend_eni",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:network-interface/eni-01ef04d86b620823f",
            "attachment": [],
            "description": "",
            "id": "eni-01ef04d86b620823f",
            "interface_type": "interface",
            "ipv4_prefix_count": 0,
            "ipv4_prefixes": [],
            "ipv6_address_count": 0,
            "ipv6_address_list": [],
            "ipv6_address_list_enabled": false,
            "ipv6_addresses": [],
            "ipv6_prefix_count": 0,
            "ipv6_prefixes": [],
            "mac_address": "02:5b:80:8f:01:14",
            "outpost_arn": "",
            "owner_id": "741286671880",
            "private_dns_name": "",
            "private_ip": "10.0.2.100",
            "private_ip_list": [
              "10.0.2.100"
            ],
            "private_ip_list_enabled": false,
            "private_ips": [
              "10.0.2.100"
            ],
            "private_ips_count": 0,
            "security_groups": [
              "sg-022088b490b9408f7"
            ],
            "source_dest_check": true,
            "subnet_id": "subnet-055b001d04c88d3f8",
            "tags": {
              "Name": "my_vpc-private_subnet-backend_eni"
            },
            "tags_all": {
              "Name": "my_vpc-private_subnet-backend_eni"
            }
          },
          "sensitive_attributes": [],
          "private": "bnVsbA==",
          "dependencies": [
            "aws_security_group.private_sg_backend",
            "aws_subnet.private_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_network_interface",
      "name": "private_db_eni",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:network-interface/eni-07d4bd086558a35cf",
            "attachment": [],
            "description": "",
            "id": "eni-07d4bd086558a35cf",
            "interface_type": "interface",
            "ipv4_prefix_count": 0,
            "ipv4_prefixes": [],
            "ipv6_address_count": 0,
            "ipv6_address_list": [],
            "ipv6_address_list_enabled": false,
            "ipv6_addresses": [],
            "ipv6_prefix_count": 0,
            "ipv6_prefixes": [],
            "mac_address": "02:f0:a2:78:d8:f8",
            "outpost_arn": "",
            "owner_id": "741286671880",
            "private_dns_name": "",
            "private_ip": "10.0.2.101",
            "private_ip_list": [
              "10.0.2.101"
            ],
            "private_ip_list_enabled": false,
            "private_ips": [
              "10.0.2.101"
            ],
            "private_ips_count": 0,
            "security_groups": [
              "sg-0c9c6b9b6ef85e094"
            ],
            "source_dest_check": true,
            "subnet_id": "subnet-055b001d04c88d3f8",
            "tags": {
              "Name": "my_vpc-private_subnet-db_eni"
            },
            "tags_all": {
              "Name": "my_vpc-private_subnet-db_eni"
            }
          },
          "sensitive_attributes": [],
          "private": "bnVsbA==",
          "dependencies": [
            "aws_security_group.private_sg_db",
            "aws_subnet.private_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_network_interface",
      "name": "public_frontend_eni",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:network-interface/eni-07e5b8e7d2fea4a5f",
            "attachment": [],
            "description": "",
            "id": "eni-07e5b8e7d2fea4a5f",
            "interface_type": "interface",
            "ipv4_prefix_count": 0,
            "ipv4_prefixes": [],
            "ipv6_address_count": 0,
            "ipv6_address_list": [],
            "ipv6_address_list_enabled": false,
            "ipv6_addresses": [],
            "ipv6_prefix_count": 0,
            "ipv6_prefixes": [],
            "mac_address": "02:3a:4d:6f:8a:d6",
            "outpost_arn": "",
            "owner_id": "741286671880",
            "private_dns_name": "",
            "private_ip": "10.0.1.100",
            "private_ip_list": [
              "10.0.1.100"
            ],
            "private_ip_list_enabled": false,
            "private_ips": [
              "10.0.1.100"
            ],
            "private_ips_count": 0,
            "security_groups": [
              "sg-027341e29aaa542b7"
            ],
            "source_dest_check": true,
            "subnet_id": "subnet-02e521fe0f56d67b6",
            "tags": {
              "Name": "my_vpc-public_subnet-frontend_eni"
            },
            "tags_all": {
              "Name": "my_vpc-public_subnet-frontend_eni"
            }
          },
          "sensitive_attributes": [],
          "private": "bnVsbA==",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_security_group.public_sg",
            "aws_subnet.public_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_route_table",
      "name": "internet_connection",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:route-table/rtb-0dfcc789d23d76219",
            "id": "rtb-0dfcc789d23d76219",
            "owner_id": "741286671880",
            "propagating_vgws": [],
            "route": [
              {
                "carrier_gateway_id": "",
                "cidr_block": "0.0.0.0/0",
                "destination_prefix_list_id": "",
                "egress_only_gateway_id": "",
                "gateway_id": "igw-041c43cb10c8856bf",
                "instance_id": "",
                "ipv6_cidr_block": "",
                "local_gateway_id": "",
                "nat_gateway_id": "",
                "network_interface_id": "",
                "transit_gateway_id": "",
                "vpc_endpoint_id": "",
                "vpc_peering_connection_id": ""
              }
            ],
            "tags": {
              "Name": "my_vpc-nat_gw-internet_connection"
            },
            "tags_all": {
              "Name": "my_vpc-nat_gw-internet_connection"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjozMDAwMDAwMDAwMDAsImRlbGV0ZSI6MzAwMDAwMDAwMDAwLCJ1cGRhdGUiOjEyMDAwMDAwMDAwMH19",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_route_table_association",
      "name": "internet_public_subnet",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "gateway_id": "",
            "id": "rtbassoc-0028d4809c18113a7",
            "route_table_id": "rtb-0dfcc789d23d76219",
            "subnet_id": "subnet-02e521fe0f56d67b6"
          },
          "sensitive_attributes": [],
          "private": "bnVsbA==",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_route_table.internet_connection",
            "aws_subnet.public_subnet",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_security_group",
      "name": "private_sg_backend",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:security-group/sg-022088b490b9408f7",
            "description": "Private VPC Security Group - Backend",
            "egress": [
              {
                "cidr_blocks": [
                  "10.0.1.100/32"
                ],
                "description": "",
                "from_port": 3000,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 3000
              },
              {
                "cidr_blocks": [
                  "10.0.2.101/32"
                ],
                "description": "",
                "from_port": 0,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "-1",
                "security_groups": [],
                "self": false,
                "to_port": 0
              }
            ],
            "id": "sg-022088b490b9408f7",
            "ingress": [
              {
                "cidr_blocks": [
                  "10.0.0.0/16"
                ],
                "description": "",
                "from_port": 8,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "icmp",
                "security_groups": [],
                "self": false,
                "to_port": 0
              },
              {
                "cidr_blocks": [
                  "10.0.1.0/24"
                ],
                "description": "",
                "from_port": 22,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 22
              },
              {
                "cidr_blocks": [
                  "10.0.1.0/24"
                ],
                "description": "",
                "from_port": 3000,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 3000
              }
            ],
            "name": "private_sg_backend",
            "name_prefix": "",
            "owner_id": "741286671880",
            "revoke_rules_on_delete": false,
            "tags": {
              "Description": "Private VPC Security Group - Backend",
              "Name": "private_sg_backend"
            },
            "tags_all": {
              "Description": "Private VPC Security Group - Backend",
              "Name": "private_sg_backend"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6OTAwMDAwMDAwMDAwfSwic2NoZW1hX3ZlcnNpb24iOiIxIn0=",
          "dependencies": [
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_security_group",
      "name": "private_sg_db",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:security-group/sg-0c9c6b9b6ef85e094",
            "description": "Private VPC Security Group - Database",
            "egress": [
              {
                "cidr_blocks": [
                  "10.0.2.100/32"
                ],
                "description": "",
                "from_port": 22,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 22
              },
              {
                "cidr_blocks": [
                  "10.0.2.100/32"
                ],
                "description": "",
                "from_port": 27017,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 27017
              }
            ],
            "id": "sg-0c9c6b9b6ef85e094",
            "ingress": [
              {
                "cidr_blocks": [
                  "10.0.0.0/16"
                ],
                "description": "",
                "from_port": 8,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "icmp",
                "security_groups": [],
                "self": false,
                "to_port": 0
              },
              {
                "cidr_blocks": [
                  "10.0.2.100/32"
                ],
                "description": "",
                "from_port": 22,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 22
              },
              {
                "cidr_blocks": [
                  "10.0.2.100/32"
                ],
                "description": "",
                "from_port": 27017,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 27017
              }
            ],
            "name": "private_sg_db",
            "name_prefix": "",
            "owner_id": "741286671880",
            "revoke_rules_on_delete": false,
            "tags": {
              "Description": "Private VPC Security Group - Database",
              "Name": "private_sg_db"
            },
            "tags_all": {
              "Description": "Private VPC Security Group - Database",
              "Name": "private_sg_db"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6OTAwMDAwMDAwMDAwfSwic2NoZW1hX3ZlcnNpb24iOiIxIn0=",
          "dependencies": [
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_security_group",
      "name": "public_sg",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:security-group/sg-027341e29aaa542b7",
            "description": "Public VPC Security Group",
            "egress": [
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "",
                "from_port": 0,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "-1",
                "security_groups": [],
                "self": false,
                "to_port": 0
              }
            ],
            "id": "sg-027341e29aaa542b7",
            "ingress": [
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "",
                "from_port": 22,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 22
              },
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "",
                "from_port": 8080,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "tcp",
                "security_groups": [],
                "self": false,
                "to_port": 8080
              },
              {
                "cidr_blocks": [
                  "0.0.0.0/0"
                ],
                "description": "",
                "from_port": 8,
                "ipv6_cidr_blocks": [],
                "prefix_list_ids": [],
                "protocol": "icmp",
                "security_groups": [],
                "self": false,
                "to_port": 0
              }
            ],
            "name": "public_sg",
            "name_prefix": "",
            "owner_id": "741286671880",
            "revoke_rules_on_delete": false,
            "tags": {
              "Description": "Public VPC Security Group",
              "Name": "public_sg"
            },
            "tags_all": {
              "Description": "Public VPC Security Group",
              "Name": "public_sg"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6OTAwMDAwMDAwMDAwfSwic2NoZW1hX3ZlcnNpb24iOiIxIn0=",
          "dependencies": [
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_subnet",
      "name": "private_subnet",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:subnet/subnet-055b001d04c88d3f8",
            "assign_ipv6_address_on_creation": false,
            "availability_zone": "ap-southeast-1b",
            "availability_zone_id": "apse1-az1",
            "cidr_block": "10.0.2.0/24",
            "customer_owned_ipv4_pool": "",
            "enable_dns64": false,
            "enable_resource_name_dns_a_record_on_launch": false,
            "enable_resource_name_dns_aaaa_record_on_launch": false,
            "id": "subnet-055b001d04c88d3f8",
            "ipv6_cidr_block": "",
            "ipv6_cidr_block_association_id": "",
            "ipv6_native": false,
            "map_customer_owned_ip_on_launch": false,
            "map_public_ip_on_launch": false,
            "outpost_arn": "",
            "owner_id": "741286671880",
            "private_dns_hostname_type_on_launch": "ip-name",
            "tags": {
              "Name": "my_vpc-private_subnet"
            },
            "tags_all": {
              "Name": "my_vpc-private_subnet"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMH0sInNjaGVtYV92ZXJzaW9uIjoiMSJ9",
          "dependencies": [
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_subnet",
      "name": "public_subnet",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:subnet/subnet-02e521fe0f56d67b6",
            "assign_ipv6_address_on_creation": false,
            "availability_zone": "ap-southeast-1b",
            "availability_zone_id": "apse1-az1",
            "cidr_block": "10.0.1.0/24",
            "customer_owned_ipv4_pool": "",
            "enable_dns64": false,
            "enable_resource_name_dns_a_record_on_launch": false,
            "enable_resource_name_dns_aaaa_record_on_launch": false,
            "id": "subnet-02e521fe0f56d67b6",
            "ipv6_cidr_block": "",
            "ipv6_cidr_block_association_id": "",
            "ipv6_native": false,
            "map_customer_owned_ip_on_launch": false,
            "map_public_ip_on_launch": true,
            "outpost_arn": "",
            "owner_id": "741286671880",
            "private_dns_hostname_type_on_launch": "ip-name",
            "tags": {
              "Name": "my_vpc-public_subnet"
            },
            "tags_all": {
              "Name": "my_vpc-public_subnet"
            },
            "timeouts": null,
            "vpc_id": "vpc-0041dec4050ba4369"
          },
          "sensitive_attributes": [],
          "private": "eyJlMmJmYjczMC1lY2FhLTExZTYtOGY4OC0zNDM2M2JjN2M0YzAiOnsiY3JlYXRlIjo2MDAwMDAwMDAwMDAsImRlbGV0ZSI6MTIwMDAwMDAwMDAwMH0sInNjaGVtYV92ZXJzaW9uIjoiMSJ9",
          "dependencies": [
            "aws_internet_gateway.nat_gw",
            "aws_vpc.my_vpc"
          ]
        }
      ]
    },
    {
      "mode": "managed",
      "type": "aws_vpc",
      "name": "my_vpc",
      "provider": "provider[\"registry.terraform.io/hashicorp/aws\"]",
      "instances": [
        {
          "schema_version": 1,
          "attributes": {
            "arn": "arn:aws:ec2:ap-southeast-1:741286671880:vpc/vpc-0041dec4050ba4369",
            "assign_generated_ipv6_cidr_block": false,
            "cidr_block": "10.0.0.0/16",
            "default_network_acl_id": "acl-0760713c6e8305b5d",
            "default_route_table_id": "rtb-06b4d85facd77de9e",
            "default_security_group_id": "sg-090e754755f17a989",
            "dhcp_options_id": "dopt-0c625bf1d01ca710c",
            "enable_classiclink": false,
            "enable_classiclink_dns_support": false,
            "enable_dns_hostnames": false,
            "enable_dns_support": true,
            "id": "vpc-0041dec4050ba4369",
            "instance_tenancy": "default",
            "ipv4_ipam_pool_id": null,
            "ipv4_netmask_length": null,
            "ipv6_association_id": "",
            "ipv6_cidr_block": "",
            "ipv6_cidr_block_network_border_group": "",
            "ipv6_ipam_pool_id": "",
            "ipv6_netmask_length": 0,
            "main_route_table_id": "rtb-06b4d85facd77de9e",
            "owner_id": "741286671880",
            "tags": {
              "Name": "my_vpc"
            },
            "tags_all": {
              "Name": "my_vpc"
            }
          },
          "sensitive_attributes": [],
          "private": "eyJzY2hlbWFfdmVyc2lvbiI6IjEifQ=="
        }
      ]
    }
  ]
}

```