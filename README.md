# Terraform Assignment

## Installation

Step 1: Prepare an AWS IAM account with programmatic (API) access only 

Step 2: To execute, prepare a `aws_key.tf` variable filein the root directory of this Git repository.

Furnish the following variables containing the values into the `aws_tf` file:

* `aws_access_key` - AWS IAM Access ID created in Step 1
* `aws_secret_key` - AWS IAM Secret Key created in Step 1

## Schematic
[Link to Diagram](https://drive.google.com/file/d/1btmwXvy9rz90x8YzjB-yVyzYDKFZB04C/view?usp=sharing)

## Sample `aws_key.tf` file
[Link to aws_key sample file](sample_aws_key.md)

## Sample `tfstate` file
[Link to tfstate sample file](sample_tfstate.md)

